#!//data/com.termux/files/usr/bin/bash

# to execute this script
# bash <(curl -fsSL https://gitlab.com/mick842-public/rms/print-service/-/raw/main/scripts/init.sh) https://site.com/api/trpc

if [ "$#" -eq 0 ]; then
    echo "Please provide API_URL"
    exit 1
fi

# Accessing the API_URL argument
API_URL=$1

# Initial setup
apt update && apt upgrade -y
apt install -y nodejs git openssh cronie termux-services

# Install and update pm2
npm install pm2 -g
pm2 update

APP_PATH="$HOME/printService"
GIT_REPO="https://gitlab.com/mick842-public/rms/print-service.git"

rm -rf $APP_PATH
mkdir -p $APP_PATH

cd $APP_PATH && git clone $GIT_REPO .

echo "echo \"0 */8 * * * API_URL=$API_URL npm --prefix $APP_PATH run pm2\" | crontab -" > ~/.bash_profile

echo >> ~/.bash_profile
echo 'if ! pgrep -f "crond" >/dev/null; then' >> ~/.bash_profile
echo 'echo "[Starting crond...]" && crond && echo "[OK]"' >> ~/.bash_profile
echo 'else' >> ~/.bash_profile
echo 'echo "[crond is running]"' >> ~/.bash_profile
echo 'fi' >> ~/.bash_profile
echo >> ~/.bash_profile

echo "API_URL=$API_URL npm --prefix $APP_PATH run pm2" >> ~/.bash_profile

source ~/.bash_profile
