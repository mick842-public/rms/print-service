import net from "net";

const server = net.createServer((socket) => {
  console.log("Client connected");

  socket.on("data", function (data) {
    console.log("Received: ", data.toString());
    socket.write("OK");
  });
});

server.listen(9100, () => {
  console.log("Server listening on port 9100");
});
