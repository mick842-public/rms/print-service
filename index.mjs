import {
  ThermalPrinter,
  PrinterTypes,
  CharacterSet,
} from "node-thermal-printer";
import pkg from "./package.json" assert { type: "json" };

const apiUrl = process.env.API_URL;
const nextApiUrl = apiUrl.replace("/trpc", "");
const checkPrintJobsInterval =
  Number(process.env.CHECK_PRINT_JOBS_INTERVAL) || 10 * 1000; // 10 seconds
const checkPrinterStatusInterval =
  Number(process.env.CHECK_PRINTER_STATUS_INTERVAL) || 60 * 1000; // 60 seconds

let enableCheckPrintJobs = true;
let printerParams = {
  address: "",
  // isOnline: false,
  // isPrinting: false,
  // lastUpdated: null,
};

const logError = async (error) => {
  try {
    return await fetch(apiUrl + "/logPrintError", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        message: error?.message,
        error: typeof error === "string" ? error : error.toString(),
      }),
    });
  } catch (e) {
    console.error("Failed to log error: ", e);
  }
};

async function testPrint(printer) {
  console.log("starting test print");
  let isConnected = await printer.isPrinterConnected();

  console.log("isConnected:", isConnected);

  if (!isConnected) {
    throw "Printer is not connected";
  }

  const width = printer.getWidth();
  console.log({ width });

  printer.println("Width: " + width);

  const status = await printer.getStatus?.();
  console.log({ status });

  printer.print("Print without break1");
  printer.print("Print without break2");
  printer.print("Print without break3");

  printer.newLine();

  printer.println("Print with break1");
  printer.println("Print with break2");
  printer.println("Print with break3");

  printer.newLine();

  printer.bold(true);
  printer.print("bold");
  printer.bold(false);

  printer.invert(true);
  printer.print("invert");
  printer.invert(false);

  printer.underline(true);
  printer.print("underline");
  printer.underline(false);

  printer.newLine();

  printer.underlineThick(true);
  printer.print("underlineThick");
  printer.underlineThick(false);

  printer.newLine();

  printer.println("Draw line:");
  printer.drawLine();
  printer.newLine();

  printer.alignCenter();
  printer.println("Align center");

  printer.alignRight();
  printer.println("Align right");

  printer.alignLeft();

  printer.newLine();

  printer.setTypeFontA();
  printer.println("Type font A");

  printer.setTypeFontB();
  printer.println("Type font B");

  printer.setTypeFontA();

  printer.setTextDoubleHeight();
  printer.println("Double height");
  printer.setTextNormal();

  printer.setTextDoubleWidth();
  printer.println("Double width");
  printer.setTextNormal();

  printer.setTextQuadArea();
  printer.println("Quad area");
  printer.setTextNormal();

  printer.newLine();

  printer.leftRight("Left", "Right");
  printer.newLine();

  printer.table(["One", "Two", "Three"]);
  printer.newLine();

  printer.tableCustom([
    { text: "Left", align: "LEFT", width: 0.5 },
    { text: "Center", align: "CENTER", width: 0.25, bold: true },
    { text: "Right", align: "RIGHT", cols: 8 },
  ]);
  printer.newLine();

  printer.alignCenter();

  printer.printQR("QR CODE", {
    cellSize: 3,
  });
  printer.newLine();
  printer.printQR("QR CODE", {
    cellSize: 5,
  });
  printer.newLine();
  printer.printQR("QR CODE", {
    cellSize: 8,
  });
  printer.newLine();

  try {
    await printer.printImage("./img/logo.png");
  } catch (e) {
    console.error("printing image failed:", e);
  }
}

async function testPaymentQRCodePrint(printer) {
  const qrCodeImageBase64 = await fetch(
    `${nextApiUrl}/getPaymentQR?variant=bw&size=400&code=00020101021230810016A00000067701011201150107536000315080214KB1020571497040320API1582994752120378531690016A00000067701011301030040214KB1020571497040420API1582994752120378553037645406750.005802TH6304854e`,
  )
    .then((res) => res.json())
    .then((res) => res.qrCode);
  const qrCodeImageBuffer = Buffer.from(qrCodeImageBase64, "base64");

  printer.alignCenter();
  printer.printImageBuffer(qrCodeImageBuffer);
  printer.println("Amount: 123฿");

  // Current date + 9 minutes
  const currentDatePlus9Minutes = new Date();
  currentDatePlus9Minutes.setMinutes(currentDatePlus9Minutes.getMinutes() + 9);

  printer.println(
    "Pay by " +
      new Intl.DateTimeFormat("en", {
        hour: "2-digit",
        minute: "2-digit",
        hour12: false,
      }).format(currentDatePlus9Minutes),
  );
  printer.alignLeft();
}

async function print() {
  let currentPrintJobId;

  try {
    enableCheckPrintJobs = false;

    const printJob = await fetch(apiUrl + "/getNextPrintJob").then((response) =>
      response.json(),
    );

    const printJobId = printJob?.result?.data?.id;
    const printJobBody = printJob?.result?.data?.body;

    currentPrintJobId = printJobId;

    if (!printJobBody || !printJobId) {
      enableCheckPrintJobs = true;
      return;
    }

    console.log("printing", printJobId);

    const printer = new ThermalPrinter({
      type: PrinterTypes.EPSON,
      interface: "tcp://" + printerParams.address,
      options: {
        timeout: 5000,
      },
      characterSet: CharacterSet.ISO8859_15_LATIN9,
      width: 42,
    });

    await printer.isPrinterConnected();

    await fetch(apiUrl + "/startPrintJob", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(printJobId),
    });

    printer.alignCenter();
    try {
      await printer.printImage("./img/logo.png");
    } catch (e) {
      console.error("printing image failed:", e);
    }
    printer.alignLeft();
    printer.newLine();

    const printBody = JSON.parse(printJobBody);

    for (const item of printBody) {
      if (item.type === "TEXT") {
        printer.println(item.value[0]);
      }

      if (item.type === "MENU_ITEM") {
        printer.tableCustom([
          {
            text: item.value[0] + " " + item.value[1],
            align: "LEFT",
            cols: 36,
          },
          { text: item.value[2], align: "RIGHT", cols: 6 },
        ]);
      }

      if (item.type === "MODIFIER") {
        printer.tableCustom([
          {
            text: "   " + item.value[0],
            align: "LEFT",
            cols: 36,
            // @ts-ignore
            underline: true,
          },
          { text: item.value[1], align: "RIGHT", cols: 6 },
        ]);
      }

      if (item.type === "TOTAL") {
        printer.newLine();
        printer.newLine();
        printer.bold(true);

        printer.leftRight(item.value[0], item.value[1]);

        printer.newLine();
        printer.newLine();
        printer.bold(false);
      }

      if (item.type === "SEAT_SUBTOTAL") {
        printer.newLine();
        printer.underlineThick(true);

        printer.leftRight(item.value[0], item.value[1]);

        printer.underlineThick(false);
        printer.newLine();
      }

      if (item.type === "SEAT_NUMBER") {
        const marginLeft = 3;
        const dashedString = new Array(42).fill("-").join("");
        const seatNumberString = `[${item.value[0]}]`;

        printer.println(
          dashedString.slice(0, marginLeft) +
            seatNumberString +
            dashedString.slice(marginLeft + seatNumberString.length),
        );
      }

      if (item.type === "TABLE_NAME") {
        printer.alignRight();
        printer.println(item.value[0]);
        printer.alignLeft();
      }

      if (item.type === "DATE_TIME") {
        printer.alignRight();
        printer.println(item.value[0]);
        printer.alignLeft();
      }

      if (item.type === "COMPLETED_ORDER_ITEM") {
        printer.tableCustom([
          {
            text: item.value[0],
            align: "LEFT",
            cols: 36,
          },
          { text: item.value[1], align: "RIGHT", cols: 6 },
        ]);
      }

      if (item.type === "QR_PAYMENT_CODE") {
        const qrCodeImageBase64 = await fetch(
          `${nextApiUrl}/getPaymentQR?variant=bw&size=400&code=${item.value[2]}`,
        )
          .then((res) => res.json())
          .then((res) => res.qrCode);
        const qrCodeImageBuffer = Buffer.from(qrCodeImageBase64, "base64");

        printer.alignCenter();
        printer.printImageBuffer(qrCodeImageBuffer);
        printer.println("Amount: " + item.value[0] + "฿");
        printer.println(
          "Pay by " +
            new Intl.DateTimeFormat("en", {
              hour: "2-digit",
              minute: "2-digit",
              hour12: false,
            }).format(new Date(item.value[1])),
        );
        printer.alignLeft();
      }

      if (item.type === "TEST_PRINT") {
        await testPrint(printer);
      }

      if (item.type === "TEST_PAYMENT_QR_CODE_PRINT") {
        await testPaymentQRCodePrint(printer);
      }
    }

    printer.cut();

    try {
      await printer.execute({ waitForResponse: true });
    } catch (error) {
      console.error("Print error:", printJobId, error);
      throw error;
    }

    await fetch(apiUrl + "/endPrintJob", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(printJobId),
    });
  } catch (error) {
    console.error("Print error:", error);
    await logError(error);

    try {
      await fetch(apiUrl + "/errorPrintJob", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(currentPrintJobId),
      });
    } catch (e) {}
  } finally {
    enableCheckPrintJobs = true;
  }
}

async function checkPrintJobs() {
  if (!enableCheckPrintJobs) {
    return;
  }

  const hasPrintJobs = await fetch(apiUrl + "/hasPrintJobs").then((response) =>
    response.json(),
  );

  if (hasPrintJobs?.result?.data) {
    await print();
  }
}

async function checkPrinterStatus() {
  const printerSettings = await fetch(apiUrl + "/getPrinter").then((response) =>
    response.json(),
  );

  printerParams.address = printerSettings?.result?.data?.address;

  const printer = new ThermalPrinter({
    type: PrinterTypes.EPSON,
    interface: "tcp://" + printerParams.address,
    options: {
      timeout: 5000,
    },
  });

  const result = await printer.isPrinterConnected();

  enableCheckPrintJobs = result;

  await fetch(apiUrl + "/updatePrinterStatus", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      status: result ? "ONLINE" : "OFFLINE",
      version: pkg.version,
    }),
  });
}

setInterval(checkPrintJobs, checkPrintJobsInterval);
setInterval(checkPrinterStatus, checkPrinterStatusInterval);

checkPrinterStatus();

console.log("started");
console.log("version: ", pkg.version);
