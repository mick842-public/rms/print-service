module.exports = {
  apps: [
    {
      name: "print-service",
      script: "npm",
      args: ["start"],
      ignore_watch: ["node_modules"],
    },
  ],
};
